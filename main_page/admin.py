from django.contrib import admin
from .models import *


admin.site.register(AboutEvent)
admin.site.register(Topic)
admin.site.register(Day)
admin.site.register(Speakers)
