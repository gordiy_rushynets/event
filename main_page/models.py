from django.db import models


class Speakers(models.Model):
    full_name = models.CharField(max_length=75)
    about_speaker = models.CharField(max_length=100)
    telegram_link = models.CharField(max_length=200, blank=True)
    facebook_link = models.CharField(max_length=200, blank=True)
    instagram_link = models.CharField(max_length=200, blank=True)
    linked_in_link = models.CharField(max_length=200, blank=True)
    twiter_link = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return '{0}'.format(self.full_name)

    class Meta:
        verbose_name = "Speaker"
        verbose_name_plural = "Speakers"


class Day(models.Model):
    date = models.DateField()
    day_number = models.IntegerField()

    def __str__(self):
        return 'Day nymber: {0}'.format(self.day_number)


class Topic(models.Model):
    time = models.TimeField()
    day = models.ForeignKey(Day, on_delete=models.CASCADE)
    speaker = models.ForeignKey(Speakers, on_delete=models.CASCADE, blank=True)
    topic = models.CharField(max_length = 35)
    comment = models.CharField(max_length = 250)

    def __str__(self):
        return '{0}'.format(self.topic)


class AboutEvent(models.Model):
    titile = models.CharField(max_length=50)
    date_from = models.DateField()
    date_to = models.DateField(blank=True)
    address = models.CharField(max_length=100)

    link_to_video = models.TextField()
    about_event = models.TextField()


    def __str__(self):
        return '{0}'.format(self.titile)

    class Meta:
        verbose_name = "About event"
        verbose_name_plural = 'About Event'
