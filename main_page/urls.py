from django.urls import path
from .views import about_event


urlpatterns = [
    path('', about_event, name="about_event"),
]
