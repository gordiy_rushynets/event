from django.shortcuts import render
from .models import AboutEvent


def about_event(request):
    about_event = AboutEvent.objects.all()
    return render(request, "index.html", {'about_event': about_event})

